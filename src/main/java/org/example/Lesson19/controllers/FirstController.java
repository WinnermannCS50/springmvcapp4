package org.example.Lesson19.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
/**
 * При GET--запросе на /first/hello сработает метод helloPage()
 * При GET--запросе на /first/goodbye сработает метод goodByePage()
 */
@RequestMapping("/first")
public class FirstController {

    /**
     * При GET--запросе на /hello сработает метод helloPage()
     * @return
     *
     * HttpServletRequest request - принимает все сведения об Http-запросе пользователя
     *
     * Делаем в браузере HTTP GET-запрос с параметрами name=Tom и surname=Jones после знака ?
     * http://localhost:8084/SpringMVCApp4_war_exploded/first/hello?name=Tom&surname=Jones
     */
    @GetMapping("/hello")
    public String helloPage(HttpServletRequest request){
        /**
         * Параметр_1
         * Получает значение для поля с ключем name из запроса в браузере
         */
        String name = request.getParameter("name");
        /**
         * Параметр_2
         * Получает значение для поля с ключем surname из запроса в браузере
         */
        String surname = request.getParameter("surname");

        System.out.println("Hello " + name + " " + surname);
        return "first/hello";

    }

    /**
     * При GET--запросе на /goodbye сработает метод goodByePage()
     * @return
     */
    @GetMapping("/goodbye")
    public String goodByePage(){
        return "first/goodbye";

    }
}
